package client

import "github.com/xanzy/go-gitlab"

// ListUserProjects lists the projects for the user by delegating to https://github.com/xanzy/go-gitlab/blob/master/projects.go#L261
func ListUserProjects(git *gitlab.Client) ([]*gitlab.Project, error) {
	projects, _, err := git.Projects.ListUserProjects("matthewcosgrove", nil)
	if err != nil {
		return nil, err
	}
	return projects, nil
}
