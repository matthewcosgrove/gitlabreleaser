// +build integration

package client_test

import (
	"flag"
	"fmt"
	"testing"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/matthewcosgrove/gitlabreleaser/client"
)

var token = flag.String("token", "missing", "GitLab API token")

func TestListUserProjects(t *testing.T) {
	git, err := gitlab.NewClient(*token)
	if err != nil {
		t.Fatalf("Failed to create client: %v", err)
	}
	projects, err := client.ListUserProjects(git)
	if err != nil {
		t.Fatalf("Failed to ListUserProjects: %v", err)
	}
	fmt.Printf("%s\n", projects)
}
